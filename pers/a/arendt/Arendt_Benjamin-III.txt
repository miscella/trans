Hannah Arendt, »Walter Benjamin. III. Der Perlentaucher«, in Merkur 22 (April 1968), Nr. 240, H 4, pp. 305-315.

Sofern Vergangenheit als Tradition überliefert ist, hat sie Autorität; sofern Autorität sich geschichtlich darstellt, wird sie zur Tradition. Walter Benjamin wußte, daß Traditionsbruch und Autoritätsverlust irreparabel waren, und zog daraus den Schluß, neue Wege für den Umgang mit der Vergangenheit zu entdecken. In diesem Umgang wurde er ein Meister, als er entdeckte, daß an die Stelle der Tradierbarkeit der Vergangenheit ihre Zitierbarkeit getreten war, an die Stelle ihrer Autorität die gespenstische Kraft, sich stückweise in der Gegenwart anzusiedeln und ihr den falschen Frieden der gedankenlosen Selbstzufriedenheit zu rauben. (305)

Aber wenn auch ›erst der Verzweifelnde‹, nämlich der an der Gegenwart Verzweifelnde (wie Benjamin an Karl Kraus exemplifiziert) ›im Zitat die Kraft [entdeckt]: nicht zu bewahren, sondern zu reinigen, aus dem Zusammenhang zu reißen, zu zerstören‹, so ist doch dieser Entdecker des Destruktiven ursprünglich von einer ganz anderen Absicht beseelt, nämlich von der Absicht zu bewahren; und nur weil er sich nichts vormachen läßt von den berufsmäßigen ›Bewahrern‹ der Vergangenheit, der Werte, des Positiven usw., entdeckt er schließlich, daß die destruktive Kraft des Zitats ›die einzige [ist], in der noch Hoffnung liegt, daß einiges aus diesem Zeitraum überdauert — weil man es nämlich aus ihm herausschlug‹. In dieser Form von ›Denkbruchstücken‹ hat das Zitat die Aufgabe, den Fluß der Darstellung mit ›transzendenter Wucht‹ sowohl zu unterbrechen wie das Dargestellte in sich zu versammeln. An Gewicht kann es sich in Benjamins Arbeit nur mit dem ganz anders gearteten autoritären Zitat messen, das in den Traktaten des Mittelalters die immanente Stimmigkeit der Beweisführung ersetzt. (305)

… das ihm eigentümliche Flanieren auch im Geistigen, bei dem er — wie der Flaneur in der Stadt — als Führer auf seinen Entdeckungsreisen dem Zufall vertraut. (308)

… das nur ihm eigene Zitate-Sammeln… (310)

Jedenfalls war nichts für ihn in den dreißiger Jahren charakteristischer als die kleinen, schwarz-gebundenen Notizbüchlein, die er immer bei sich trug und in die er unermüdlich in Zitaten eintrug, was das tägliche Leben und Lesen ihm an ›Perlen und Korallen‹ zutrug, um sie dann gelegentlich wie Stücke einer erlesen kostbaren Sammlung vorzuzeigen und vorzulesen. (310)

[aus einem Brief an Hofmannsthal:]
›Die Überzeugung, welche in meinen literarischen Versuchen mich leitet... [ist], daß jede Wahrheit ihr Haus, ihren angestammten Palast, in der Sprache hat, daß er aus den ältesten logoi errichtet ist und daß der so gegründeten Wahrheit gegenüber die Einsichten der Einzelwissenschaften subaltern bleiben, solange sie gleichsam nomadisierend, bald hier, bald da im Sprachbereich sich behelfen, befangen in jener Anschauung vom Zeichencharakter der Sprache, der ihrer Terminologie die verantwortungslose Willkür aufprägt.‹ Die Worte nämlich sind im Sinne von Benjamins frühen sprachphilosophischen Versuchen ›das Gegenteil aller nach außen gerichteten Mitteilung‹, wie die Wahrheit überhaupt ›der Tod der Intention‹ ist. (311)

Seit dem Wahlverwandtschaften-Essay steht im Zentrum jeder Arbeit Benjamins das Zitat. Schon dadurch unterscheiden sie sich von gelehrten Abhandlungen aller Art, in denen Zitate die Aufgabe haben, Meinungen zu belegen und daher ruhig in den Anmerkungsapparat verwiesen werden können. (312)

… diese Sammlung [Ursprung-Zitat-Sammlung, DH], wie die späteren Notizbücher, war nicht eine Anhäufung von Exzerpten, welche die Niederschrift erleichtern sollten, sondern stellte bereits die Hauptarbeit dar, der gegenüber die Niederschrift sekundärer Natur war. Die Hauptarbeit bestand darin, Fragmente aus ihrem Zusammenhang zu reißen und sie neu anzuordnen, und zwar so, daß sie sich gegenseitig illuminieren und gleichsam freischwebend ihre Existenzberechtigung bewähren konnten. (312)

Es handelte sich durchaus um eine Art surrealistischer Montage. Sein Ideal, eine Arbeit herzustellen, die nur aus Zitaten bestand, also so meisterhaft montiert war, daß sie jeder begleitenden Rede entraten konnte, mag skurril und selbstzerstörerisch anmuten, war es aber so wenig wie die gleichzeitigen surrealistischen Versuche, die ähnlichen Impulsen ihre Entstehung verdanken. (312)

… die Methode, das Wesen im Zitat zu erbohren — wie man Wasser aus der unterirdischen, in der Tiefe verborgenen Quelle erbohrt. (313)

Das Zitieren ist ein Nennen, und das Nennen, nicht eigentlich das Sprechen, das Wort und nicht der Satz bringen für Benjamin Wahrheit an den Tag. (313)

Es hat seine guten Gründe, daß Benjamins frühe philosophische Interessen sich ausschließlich an Sprachphilosophie orientierten und daß ihm schließlich das zitierende Nennen zu der einzig möglichen, einzig angemessenen Art und Weise wurde, mit der Vergangenheit ohne die Hilfe der Uberlieferung umzugehen. (313)

Womit ich denn nur auf eine etwas ausführlichere Weise wiederholt habe, daß wir es bei Benjamin mit etwas zu tun haben, was nun in der Tat, wenn nicht einzigartig, so doch äußerst selten ist — mit der Gabe, 'dichterisch zu denken'. (314)

Dies Denken, genährt aus dem Heute, arbeitet mit den ›Denkbruchstücken‹, die es der Vergangenheit entreißen und um sich versammeln kann. (314)

Was dies Denken leitet, ist die Überzeugung, daß zwar das Lebendige dem Ruin der Zeit verfällt, daß aber der Verwesungsprozeß gleichzeitig ein Kristallisationsprozeß ist; … neue kristallisierte Formen und Gestalten entstehen, die, gegen die Elemente gefeit, überdauern … als ›Denkbruchstücke‹, als Fragmente oder auch als die immerwährenden ›Urphänomene‹. (314)
–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
Aleida Assmann, Erinnerungsräume. Formen und Wandlungen des kulturellen Gedächtnisses, München: C.H. Beck 1999.
–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
vgl. Abschnitt »Zur Metaphorik der Erinnerung« (149-178)
-------------------------------------
---» Erstveröffentlichung (?) in: dies., Dietrich Harth (eds.), 'Mnemosyne. Formen und Funktionen der kulturellen Erinnerung', Frankfurt/M :Fischer 1991
-------------------------------------
Einleitung (149-151)
-------------------------------------
Gedächtnis – Metapher – Medium
-------------------------------------
»enge Wechselbeziehungen zwischen den Medien und den Metaphern des Gedächtnisses« (149)

Die Metaphern, die »für die Prozesse des Erinnerns und Vergessens gefunden wurden, folgen jeweils den derzeit herrschenden materiellen Aufschreibesystemen und Speichertechnologien.« (149)

Entsprechend ist der Ausgangspunkt der Überlegungen Assmanns, »daß, wer über Erinnerung spricht, dabei nicht ohne Metaphern auskommt.« (150)

Das gilt sowohl »für literarische, pädagogische oder anderweitig vorwissenschaftliche Reflexionen«, als auch für die Wissenschaft, in der »jede neue Gedächtnis-Theorie in der Regel mit einer neuen Bild-Orientierung« einhergehe. (150)

»Das Phänomen Erinnerung verschließt sich offensichtlich direkter Beschreibung und drängt in die Metaphorik. Bilder spielen dabei die Rolle von Denkfiguren, von Modellen, die die Begriffsfelder abstecken und die Theorien orientieren. ›Metaphorik‹ ist auf diesem Gebiet deshalb nicht umschreibende, sondern den Gegenstand allererst erschließende, konstituierende Sprache.« (150)

	Tafel und Magazin

Im Anschluss an Weinrich (Typen der Gedächtnismetaphorik) unterscheidet Assmann die beiden »Zentralmetaphern« der Memoria-Metaphorik. 

Magazin
––› Gedächtnis
– artifizielles Gedächtnis

Tafel
––› Erinnerung
– natürliches Gedächtnis


	Verhältnis von Gedächtnis und Erinnerung

»Verbleiben wir auf dem Boden des alltäglichen Sprachgebrauchs, dann erscheint 'Gedächtnis' als virtuelle Fähigkeit und organisches Substrat neben 'Erinnerung' als aktuellem / Vorgang des Einprägens und Rückrufens spezifischer Inhalte. Wer sich das einmal klargemacht hat, wird zugleich feststellen, daß sich beide «Pole» nicht ohne Schaden voneinander trennen lassen. Statt Gedächtnis und Erinnerung als 'Begriffsopposition' zu definieren, sollen sie hier vielmehr als 'Begriffspaar', als komplementäre Aspekte 'eines' Zusammenhangs aufgefaßt werden, die in jedem Modell gemeinsam auftauchen.« (150f.)


	Die vierte Dimension: Zeit

Assmann unternimmt eine »systematischer Erweiterung der [von Weinrich] vorgeschlagenen Matrix um eine weitere wichtige Dimension.« (151)

»Tafel und Magazin sind räumliche Metaphern; die Tafel ist eine zweidimensionale Fläche, das Magazin impliziert einen dreidimensionalen Raum. Gedächtnis und Erinnerung sind jedoch auch grundsätzlich zeithaltige Phänomene, die ohne 'die vierte Dimension der Zeit' kaum angemessen zu denken sind. Die vorübergehende Unverfügbarkeit von Erinnerungen und ihre konstitutive Nachträglichkeit etwa können in rein räumlichen Metaphern schwer zum Ausdruck gebracht werden. Im Gegenteil suggerieren sie dauerhafte Präsenz und Zugänglichkeit, was an Erinnerungen ja gerade problematisch ist.« (151)


Schriftmetaphern: Tafel, Buch, Palimpsest (151-158)

	Schrift als Metapher

So unentbehrlich und suggestiv die Schrift als Metapher des Gedächtnisses ist, so unvollkommen und irrführend ist sie auch. Widerspricht doch die Dauerpräsenz des Niedergeschriebenen eklatant der Struktur der Erinnerung, die stets diskontinuierlich ist und Intervalle der Nicht-/präsenz notwendig einschließt. An das, was gegenwärtig ist, daran kann man sich eben nicht erinnern. Um sich erinnern zu können, muß es vorübergehend entzogen gewesen und an einem Ort deponiert sein, von wo man es wieder-holen kann. Erinnerung setzt weder Dauerpräsenz noch Dauerabsenz voraus, sondern ein Wechselverhältnis von Präsenzen und Absenzen. Die Schriftmetaphorik, die mit der zeichenförmigen Fixierung zugleich auch die permanente Lesbarkeit und Verfügbarkeit des Gedächtnisinhalts impliziert, verfehlt ebendieses Wechselverhältnis von Präsenz und Absenz in der Struktur der Erinnerung. Um der Sache näher zu kommen, müßte man das Bild einer Schrift erfinden, die, einmal niedergeschrieben, nicht sofort, sondern erst unter besonderen Bedingungen wieder lesbar wird. (153f.)

	Proust

Für De Quincey ist das Gedächtnis ein Hort unsterblicher, unvergänglicher Eindrücke. Diese sind dem Menschen zwar grundsätzlich unverfügbar, er kann sie nicht kontrollieren und regieren, aber sie sind ihm körperlich eingeschrieben. Diese Vorstellung von dauerhaften, aber unverfügbaren Erinnerungsspuren […] nimmt die Proustsche 'mémoire involontaire' vorweg, die ebenfalls mit der Vorstellung von somatischen Dauerspuren verbunden ist. Proust hielt es für ›sehr gut möglich, daß die Philosophie der Zeitungsschreiber, nach der alles dem Vergessen anheim fällt, weniger wahr ist als die entgegengesetzte, die da behauptet, daß alles irgendwie erhalten bleibt‹ [Im Schatten junger Mädchenblüte, 74]. Er sprach von einer ›Wirklichkeit, deren wahre Kenntnis wir vielleicht bis zu unserem Tode versäumen und die doch ganz einfach unser Leben ist‹. Dieses wahre Leben sehen die Menschen nicht, ›weil sie es nicht dem Licht auszusetzen versuchen, infolgedessen aber ist ihre Vergangenheit von unzähligen Photonegativen angefüllt, die ganz ungenutzt bleiben, da ihr Verstand sie nicht ›entwickelt‹ hat‹ [Widergefundene Zeit, 308].« (155)

	Freud

Wie kann die Gleichzeitigkeit der entgegengesetzten Funktionen des Bewahrens und Löschens vorgestellt werden? Wie verträgt sich ›unbegrenzte Aufnahmefähigkeit‹ mit der ›Erhaltung von Dauerspuren‹? [Freud, GW, XIV, p. 4 [Wunderblock] // vgl. Bd.II/III, p. 543: ›Von den Wahrnehmungen, die an uns herankommen, verbleibt in unserem psychischen Apparat eine Spur, die wir ›Erinnerungsspuren‹ heißen können. [...] Nun bringt es offenbar Schwierigkeiten mit sich, wenn ein und dasselbe System an seinen Elementen Veränderungen getreu bewahren und doch neuen Anlässen zu Veränderung immer frisch und aufnahmefähig entgegentreten soll.‹]

»Die Lösung des Gedächtnis-Paradoxes gelang Freud schließlich mit Hilfe einer Metapher, was zugleich zeigt, daß Bilder nicht nur poetische Umschreibungen, sondern auch Instrumente einer wissenschaftlichen Heuristik sind. Er rekonstruierte den psychischen Apparat im Schrift-Modell des sog. ›Wunderblocks‹. Diesem unscheinbaren, noch heute in Kinderzimmern gebräuchlichen Spielzeug verhalf Freud zu wissenschaftlichem Ruhm. Denn die rätselhafte Kopräsenz von Dauerspur und tabula rasa ging ihm an diesem aus drei Schichten zusammengesetzten Schreibgerät auf: Die Oberfläche besteht aus einem feinen Wachspapier, das beschrieben und überschrieben wird, darunter liegt ein Zelluloidblatt, das als ›Reizschutz‹ dient, und wiederum darunter liegt die Wachstafel, die Dauerspuren (›Besetzungsinnervationen‹) festhält, die bei günstigen Lichtverhältnissen als feine Rillen sichtbar bleiben.« (156)

	Benjamin – Entwickler-Zitat

»Proust und Benjamin haben beide dieses Moment der Unverfügbarkeit, der Absenz oder genauer: Latenz in den Mittelpunkt ihrer Gedächtnisforschungen gerückt, der eine in einer autobiographischen, der andere in einer geschichtsphilosophischen Perspektive. Die Unbestimmbarkeit des Augenblicks der Entzifferung, der Lesbarkeit hat Walter Benjamin in die Formel vom ›Jetzt der Erkennbarkeit‹ gekleidet. Er ersetzte im 20. Jahrhundert die Gedächtnis-Metapher der Schrift durch die der Photographie, als er / schrieb: ›Geschichte ist wie ein Text, in den die Vergangenheit wie auf einer lichtempfindlichen Platte Bilder eingelagert hat. Erst die Zukunft besitzt die Chemikalien, die nötig sind, um dieses Bild in aller Schärfe zu entwickeln.‹ [GS I/3, p. 1238].« (156f.)

Im umnittelbaren Anschluss an das Benjamin-›Zitat‹ heißt es bei Assmann:

»Wie beim Palimpsest, so sind auch bei der Photographie Chemikalien daran beteiligt, daß eine unsichtbare Schrift lesbar bzw. ein unsichtbares Bild sichtbar wird. Von Schrift im Sinne eines Zeichenkodes kann allerdings weder bei De Quincey noch bei Freud im strengen Sinne die Rede sein. Beide ersetzen ›Schrift‹ durch die ›Spur‹. Mit dieser Ersetzung erweitert sich das Spektrum der ›Einschreibungen‹ nicht nur wesentlich; es deckt auch neue Aufzeichnungstechniken wie die Photographie mit ab. Wir sprechen von Lichtschrift und suggerieren damit, daß auch Bilder noch aus Schreibprozessen hervorgehen. Freilich gibt es dabei niemanden mehr, der schreibt; vielmehr fungiert der technische Apparat als ein Medium, mit dessen Hilfe sich das Reale selbst ›einschreibt‹.« (157)

Susan Sontag, On Photography, 1979, p. 154: ›Eine Photographie ist nicht nur ein Bild (wie ein Gemälde ein Bild ist), eine Deutung des Realen; sie ist zugleich eine Spur, eine unmittelbare Schablone des Realen, wie eine Fußspur oder eine Totenmaske.‹ 

	Digitale Medien

»Solange die Analogmedien Photographie und Film ihre Bilder über Spuren in materielle Träger eingravierten, dominierte in der Gedächtnistheorie von Proust und Warburg bis Freud die Auffassung von der Festigkeit und Unauslöschbarkeit der Gedächtnisspuren. Im Zeitalter der digitalen Medien, die in nichts mehr gravieren, sondern Schaltungen koordinieren und Impulse fließen lassen, erleben wir bezeichnenderweise ein Abrücken von solchen Gedächtnistheorien. Gedächtnis wird nun nicht mehr als Spur und Speicher, sondern als eine plastische Masse betrachtet, die unter den wechselnden Perspektiven der Gegenwart immer wieder neu geformt wird.« (158)

Raum-Metaphern (158-165)

»[…]. Diese Gestalt verkörpert den Speicher, den unendlichen Vorrat der angesammelten Daten. Die aktive Erinnerung trägt den Namen Anamnestes. Er verkörpert die bewegliche Energie des Auffindens und Hervorholens, die den Daten aus ihrer latenten Präsenz zur Manifestation verhilft. Das Gedächtnis ist der Speicher, aus dem die Erinnerung auswählt, aktualisiert, sich bedient.« (160)

	Proust

Auch Prousts 'mémoire involontaire' ist von dem Gedächtnismodell der Tiefe her bestimmt. In seiner berühmten ›Madeleine‹-Episode ist es die Meerestiefe, nicht die Erden tiefe, von der das Vergessene geborgen wird. Ein simpler Geschmacksreiz, ausgelöst durch einen Löffel Tee mit einem aufgeweichten Gebäckstück, kann urplötzlich den Kontakt zu verborgenen Erinnerungsschichten herstellen. Der Körper wird dabei in einen ungekannten Glückszustand versetzt: ›Ich hatte aufgehört, mich mittelmäßig, zufallsbedingt, sterblich zu fühlen.‹ [Swanns Welt, 64] Der Erzähler hat hier den Zustand des Menschen als eines Zeitwesens für einen Augenblick überwunden, er erfährt einen Moment der Anamnesis, eine mystische Apokatastasis, einen Moment des Alles in Allem, der vollständigen Präsenz, der Rückerstattung aller durch die Zeit geraubten Teile und Glieder (re-membering). Doch damit ist die Episode nicht zu Ende. Nach dem Körper muß der Geist noch seinen Teil an der Erinnerung leisten: ›Ich setze die Tasse nieder und wende mich meinem Geist zu. Er muß die Wahrheit finden.‹ Die geistige Erinnerungsarbeit erweist sich als mühsam, anstrengend, langwierig, doch am Ende als erfolgreich, denn auch für Proust stellt sich heraus, daß alles Wesentliche noch erhalten ist. Von Mnemotechnik kann man bei Prousts intensiver geistiger Erinnerungsarbeit nicht sprechen, eher von einer Meditation im Sinne religiöser Exerzitien. Es muß eine lange Versuchskette durchlaufen werden, bis schließlich das richtige Zauberwort gefunden ist, das ›den Anker‹ der in der Tiefe verborgenen Erinnerung ›lichtet‹ und ihr dazu verhilft, an die Oberfläche des Bewußtseins heraufzusteigen. Proust hat unvergleichlich anschaulich beschrieben, wie der sich so Erinnernde / zugleich aktiv und passiv ist: ›(Ich) spüre, wie etwas in mir sich zitternd regt und verschiebt, wie es sich zu erheben versucht, wie es in großer Tiefe den Anker gelichtet hat; ich weiß nicht, was es ist, doch langsam steigt es in mir empor; ich spüre dabei den Widerstand und höre das
Rauschen und Raunen der durchmessenen Räume.‹ [Swanns Welt, 65]« (163f.)

Auch wenn hier der Erinnernde ganz mit sich allein ist, hat die Proustsche Erinnerungsarbeit doch nichts Solipsistisches. Auch dieses Gedächtnis interagiert, ganz im Sinne von Maurice Halbwachs, mit sozialen Rahmen. Das Gedächtnis wird angesprochen und herausgefordert von einer externen Umwelt, über die es sich seiner selbst vergewissert. Wenn dieses ›milieu de mémoire‹ verlorengeht und verstummt, verliert die Erinnerung ihren konstruktiven Widerpart und wird zu einem Phantom. Die Seismographen der Sinne schlagen noch aus und registrieren Erschütterungen, doch ist die Erinnerung, für die das Individuum der letzte Träger ist, substanzlos, weil einsam geworden. Die folgende Passage kontrastiert deutlich mit dem archäologischen Optimismus eines Freud:

›Aber wenn von einer früheren Vergangenheit nichts existiert nach dem Ableben der Personen, dem Untergang der Dinge, so werden allein, zerbrechlicher aber lebendiger, immateriell und doch haltbar, beständig und treu Geruch und Geschmack noch lange wie irrende Seelen ihr Leben weiterfuhren, sich erinnern, warten, hoffen, auf den Trümmern alles übrigen und in einem beinahe unwirklich winzigen Tröpfchen das unermeßliche Gebäude der Erinnerung in sich tragen.‹ [Swanns Welt, 67] (164)

	Benjamin

Freuds Parallelisierung von Analytiker und Archäologe hat den Nachteil, daß sie eine Arbeitsteilung zwischen dem aktiven Anteil des Analytikers und dem passiven des Analysanden nahelegt. Walter Benjamin, der eines seiner sogenannten Denkbilder unter den Titel ›Ausgraben und Erinnern‹ gestellt hat, umgeht eine solche Gegenüberstellung von aktiv und passiv durch die Einführung einer dritten Kategorie, des Mediums. Im Begriff des Mediums berühren sich aktive Rekonstruktion und passive Disposition:

›Die Sprache hat es unmißverständlich bedeutet, daß das Gedächtnis nicht ein Instrument für die Erkundung des Vergangenen ist, vielmehr das Medium. Es ist das Medium des Erlebten wie das Erdreich das Medium ist, in dem die alten Städte verschüttet liegen. Wer sich der eigenen verschütteten Vergangenheit zu nähern trachtet, muß sich verhalten wie ein Mann, der gräbt. Vor allem darf er sich nicht scheuen, immer wieder auf einen und denselben Sachverhalt zurückzukommen
– ihn auszustreuen wie man Erde ausstreut, ihn umzuwühlen, wie man Erdreich umwühlt.( ... ) So müssen wahrhafte Erinnerungen viel weniger / berichtend verfahren als genau den Ort bezeichnen, an dem der Forscher ihrer
habhaft wurde.‹ [IV, 400] (164f.)

Erinnerungen, das macht Benjamin mit diesem Bild deutlich, haben keinen faktisch objektiven Charakter; auch nachdem sie aus Schichten und Querlagen herausgeschält und geborgen wurden, lassen sie sich von diesem Milieu niemals vollständig ablösen. (165)


Zeitliche Gedächtnis-Metaphern (165-178)

»Noch für Hegel wie später für Benjamin ist Erwachen der ›exemplarische Fall des Erinnerns‹. (Hegel, GW, Hamburg 1969, IV, 491 – Wo bei Benjamin?) (170)


	Schlussabschnitt

»Das Problem des Gedächtnisses, soviel ist durch die Reihe und Vielfalt der vorgestellten Beispiele deutlich geworden, drängt in die Bilder. Diese Bilder sind im Sinne von Walter Benjamin als Denk-Bilder aufzufassen, die das überkomplexe Phänomen von immer neuen Seiten zu beleuchten versuchen.« (177)

»Die Bilder unterscheiden sich grundsätzlich darin, ob sie die Eigenschaften und Verfahren des ›künstlichen‹ (ars) oder des ›natürlichen‹ Gedächtnisses (vis) erhellen. Die Unterschiede sind erheblich.Während die Gedächtniskunst der Mnemotechnik ebenso wie die technischen Verfahren der Speicherung darauf angelegt sind, daß der Gedächtnisinhalt der Einlagerung mit dem der Rückholung identisch ist, fallen beim natürlichen Gedächtnis beide Akte auseinander. (177)

»Erfahrung und Erinnerung lassen sich niemals völlig zur Übereinstimmung bringen. Zwischen beiden liegt ein Hiat, in dem der Gedächtnisinhalt verschoben, vergessen, verstellt, neu aufgeladen oder rekonstruiert wird.« (177)

»Je mehr die Gedächtnismetaphern dieser immanenten Dynamik von Erinnerungen Rechnung tragen, desto mehr heben sie die Zeitdimension als entscheidenden Faktor hervor und machen sie die Wiederherstellung zum eigentlichen Problem.« (177)

»Zwischen den räumlichen Gedächtnismodellen und den eminent zeithaften stehen jene, die sich am Bild der Schrift oder Spur orientieren. Zeitlos sind sie darin, daß sie die Prägung einer unvergänglichen Spur voraussetzen, und zeithaft insofern, als sie das Problem des vorübergehenden Verlusts, das Vergessen und die Anstrengung der Wiederherstellung thematisieren.« (177)

»In der beständigen Modernisierung der Bilder folgte auf das alphabetische Schreiben die Licht- oder Schattenschrift der Photographie als neue Leitmetapher, doch auch sie behielt nicht das letzte Wort. Die Bildlichkeit von Spur, Einprägung, Einschreibung ist inzwischen von materiellen auf elektronische Träger übergegangen.« (178)

Der letzte Paradigmawechsel unserer Leitmetaphern fuhrt zum Netzwerk, das im Vergleich zu den hier besichtigten Bildern eine entsinnlichte, ausgehöhlte Metapher ist. Im Netzwerk reduziert sich Schreiben auf die Manipulation elektronischer Impulse, was für den Computer ebenso wie für das menschliche Gehirn gilt. Es könnte sein, daß, nachdem sich Technik und Physiologie so nahe gekommen sind, der Abstand praktisch geschlossen ist, den die Vielfalt der Bilder zu überbrücken strebte. Das Netzwerk als externalisiertes globales Nervensystem unter- und überbietet zugleich alle Metaphern, die bislang das Phänomen der Erinnerung in technische, gegenständliche und praktische Metaphern übersetzten. Mit dem Netz ist die Metaphorik der Erinnerung an eine Grenze gekommen, an der die Imagination implodiert. Wer von diesem Punkt ausgeht, dem muß der hier unternommene Rundgang durch das historische Museum der Erinnerungs-Metaphern um so bemerkenswerter erscheinen. (178)





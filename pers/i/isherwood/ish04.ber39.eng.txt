----------------------------------------
ish04.ber39.eng
----------------------------------------
1
--------------------
I am a camera with its shutter open, quite passive, recording, not thinking. Recording the man shaving at the window opposite and the woman in the kimono washing her hair. Some day, all this will have to be developed, carefully printed, fixed.
--------------------
13
----------------------------------------
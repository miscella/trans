„Es gibt eine zarte Empirie, die sich mit dem Gegenstand innigst identisch macht, und dadurch zur eigentlichen Theorie wird. Diese Steigerung des geistigen Vermögens aber gehört einer hochgebildeten Zeit an.“

– Sprüche in Prosa 167, Maximen und Reflexionen 509.
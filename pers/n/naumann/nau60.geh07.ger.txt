----------------------------------------
nau60.geh07.ger
----------------------------------------
1
--------------------
Es gibt im Zeitalter des erdumspannenden Telegraphen eine Anzahl von Vorkommnissen, die überall miterlebt werden, wo überhaupt Zeitungen existieren.
--------------------
759
----------------------------------------
2
--------------------
Sehr groß ist bis jetzt die Quantität der Begebenheiten noch nicht, die überall gelesen werden, aber schon die Tatsache, daß es Nachrichten gibt, die in derselben Woche auf der ganzen Erde von allen lesenden Menschen aufgenommen werden, ist etwas neues in der Menschheitsgeschichte.
--------------------
759
----------------------------------------
3
--------------------
Damit beginnt der Begriff “Menschheit” eine psychologische Wirklichkeit zu werden.
--------------------
759
----------------------------------------
4
--------------------
Noch handelt es sich nicht um gemeinsames Handeln, aber es gibt gemeinsame Vorstellungen, und in dem Maße, in welchem diese wachsen, müssen gemeinsame Urteile, Gefühle und Handlungen sich im Laufe der Zeit einstellen.
--------------------
759
----------------------------------------
5
--------------------
Noch ist vieles ungreifbar, gestaltlos, erst noch im Zustande des Nebels.
--------------------
760
----------------------------------------
6
--------------------
Es unterliegt aber wohl keinem Zweifel, daß für eine spätere Zeit diese Erscheinungen unserer Tage eine besondere Wichtigkeit haben werden, wenigstens dann, wenn die jetzigen Anfänge eines Gehirns der Menschheit sich geradlinig weiter fortsetzen.
--------------------
760
----------------------------------------
7
--------------------
Alles was der Zukunft diente, tritt dann in schärfere Beleuchtung.
--------------------
760
----------------------------------------
8
--------------------
Was heute als Utopie und Spielerei erscheint, kann später als eine Art von Weissagung gelten, – es kann; eine Garantie dafür vermag niemand zu geben, da der gleichmäßige Fortschritt der kapitalistischen Zivilisation keineswegs über alle Zweifel erhaben ist.
--------------------
760
----------------------------------------
9
--------------------
Es kann Rückschläge geben, die wir “Weltkrieg” zu nennen pflegen, ohne doch ihre möglichen Schrecken ausdenken zu können.
--------------------
760
----------------------------------------
10
--------------------
... das Gehirn der Menschheit, das heißt eine Gemeinschaft des Denkens und Wollens oberhalb der vorhandenen Staatskörper, ... etwas, was der katholischen Kirche im Mittelalter vergleichbar sein wird, aber viel größere Ausdehnung gewinnt. ... Jetzt ist sie das alte Gehirn der Menschheit, als solches noch keineswegs wirkungslos, aber längst nicht elastisch genug, um die Menschheitsgesinnung der Zukunft zu schaffen. Es wird etwas neues gesucht, was als geistige Macht überall empfunden wird, wo es Poststationen gibt, dieses Neue aber ist viel schwerer organisierbar, als es die katholische Kirche des Mittelalters war.
--------------------
760-761
----------------------------------------
11
--------------------
Je jünger und neuer ... eine Organisation ist, desto biegsamer und flüssiger sind natürlich alle Formen. Das trifft selbstverständlich im höchsten Grade beim Gehirn der Menschheit zu. Die Oberfläche dieses Gehirns ist noch ganz weich.
--------------------
761
----------------------------------------
12
--------------------
Es gibt verschiedene Knotenpunkte des neuen internationalen Nervensystems und der Geist der Menschheit springt hinüber und herüber, bald hier, bald dort sich heimlich machen.
--------------------
761
----------------------------------------
13
--------------------
Die telegraphische Elastizität ist das Hauptkennzeichen aller neuen Organisationen.
--------------------
761
----------------------------------------
14
--------------------
Es ist ... möglich, daß die Menschen ihr eigenes kleines Einzelgehirn besser verstehen werden, wenn sie sich gewöhnt haben, die Entstehung des Gehirns der Menschheit zu beobachten.
--------------------
762
----------------------------------------

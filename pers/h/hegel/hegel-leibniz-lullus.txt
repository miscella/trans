hegel, wissenschaft der logik.
    teil II: die subjektive logik oder die lehre vom begriff
        drittes kapitel: der schluß
            a der schluss des daseins
            d. die vierte figur: a - a - a - oder der mathematische schluß
                anhang

Diese Leibnizische Anwendung des kombinatorischen Kalküls auf den Schluß und auf die Verbindung anderer Begriffe unterschied sich von der verrufenen 'Lul-/lianischen Kunst' durch nichts, als daß sie von Seiten der 'Anzahl' methodischer war, übrigens an Sinnlosigkeit ihr gleichkam. –
Es hing hiermit ein Lieblingsgedanke Leibnizens zusammen, den er in der Jugend gefaßt und der Unreifheit und Seichtigkeit desselben unerachtet auch späterhin nicht aufgab, von einer 'allgemeinen Charakteristik' der Begriffe, – einer Schriftsprache, worin jeder Begriff dargestellt werde, wie er eine Beziehung aus anderen ist oder sich auf andere beziehe, – als ob in der vernünftigen Verbindung, welche wesentlich dialektisch ist, ein Inhalt noch dieselben Bestimmungen behielte, die er hat, wenn er für sich fixiert ist. (378/9)

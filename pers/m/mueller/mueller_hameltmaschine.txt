–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
Heiner Müller, Die Hamletmaschine, in Theater heute 18 (1977), Nr. 12, pp. 39-41.
–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
Ich bin die Schreibmaschine. (40)
...
Ich bin mein Gefangener. Ich füttere mit meinen Daten die Computer. (40)
...
Ich bin die Datenbank. Blutend in der Menge. (40)
...
Ich will eine Maschine sein. (40)

Heiner Müller, Werke, hrsg. v. Frank Hörnigk, Bd. 4, Die Stücke 2, Frankfurt/M: 2001, 543 (XXX).

Müller, Heiner: Die Hamletmaschine. In: Ders.: Mauser. Texte, Band 6. Berlin, 1978, S. 89-97.

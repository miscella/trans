Leonardo Mosso:
›für eine architektur als organismus
für die selbstverwaltung der form
das gedächtnis des computers
für die programmierte
und direkte von ihren bewohnern geformte stadt
informativ unbestimmterweise programmierte architektur
wo jeder teil des ganzen
objektiv die gleiche bedeutung hat
und folglich den gegenseitigen austausch im rahmen eines
superkomplexen jedoch absolut kontrollierbaren systems
mit möglichen mutationen akzeptiert.‹

cit. Umbro Apollonio/ Carlo Belloli (ed.), Leonardo Mosso. Programmierte Architektur, Turin: Studio di informazione estetica/ Mailand: Scheiwiller 1969.
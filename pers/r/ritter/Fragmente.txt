'Fragmente aus dem Nachlass eines jungen Physikers. Ein Taschenbuch für Freunde der Natur. Hrsg. v. J W. Ritter [Herausgeberschaft fingiert!]. Zweytes Bändchen', Heidelberg: Mohr und Zimmer 1810.
------------------------------------------------------

608. (1806) Der erste Buchstabe aller Alphabete ist Menschenleben selbst, — der Odem. Erst am ›letzten‹ geht dieser über — zum Echo, und wird hörbar, — (dem Autor wie dem Publikum). [schwarzer Kreis mit schwarzem Kreuz] ist auch beste Symbole des Tons. Man darf allenfalls rein akustisch konstruieren. O wirft zurück nach a (in [schwarzer Kreis mit schwarzem Kreuz, linkes Viertel ein a] = (aX)), was von a ausging, (x). Muß nicht zu dem Laut dem Odem ein Widerstand gesetzt werden, – seine ewige Konsonante? – Der Selbstlauter eben weil er ›selbst Laut‹ ist, bedarf derselben beständig; – und so gibt es nur Sylben, wie in der Musik nur Accorde. (192f.)



»Wie es eine 'allgemeine' Sprache, und wieder 'besondere' gibt, so muß es auch eine 'allgemeine Schrift', und wieder besondere geben. Ihr nächstes Verhältnis zueinander wäre das von 'Note' zu 'Buchstabe'. Überall aber muß die Schrift das von der Sprache, dem Ton, dem Worte, selbst, Geschriebene, sein. Hier erhält man dann für die Musik, oder die 'allgemeine' Sprache, die 'Hieroglyphe', oder die völlig vollständig den ganzen Ton, den ganzen Akkord usw. ausschreibt. Das Sprechende ist dem Ausgesprochenen gleich, da alles nur sich selbst ausspricht. Die 'Sache selbst' ist also hier die Schrift, die Note. In solche Schrift und Nachschrift, 'Ab'schrift, gehört vornehmlich alle bildende Kunst: Architektur, Plastik, Malerei, usw., und was ihr in der Natur vorherging und -geht, als zum Beispiel der Bau der Erde, der Organisation der ernzeinen Orgarusatlonen, usw« (246)

– Schön wäre es, wie, was hier ä u ß e r l i c h klar würde, genau auch wäre, was u n s die. Klangfigur i n n e r l i c h ist: – L i c h t figur, F e u e r s c h r i f t .
Jeder Ton hat somit seinen / Buchstaben immediate bei sich; und es ist die Frage, ob wir nicht überhaupt nur S c h r i f t hören, – l e s e n , wenn wir hören, – Schrift s e h e n ! –
Und ist nicht jedes Sehen mit dem i n n e r n Auge H ö r e n , und Hören ein Sehen von und durch i n n e n ? (227-228)

Die so innige Verbindung von Wort und Schrift, – daß wir schreiben, wenn wir sprechen, und das Geschriebene lesbar, – hörbar, – ist, – hat mich längst beschäftigt.
Sage selbst: wie verwandelt sich uns wohl der Gedanke, die Idee ins Wort; und haben wir feinen Gedanken, oder eine Idee, ohne ihre Hieroglyphe, ihren Buchstaben, ihre Schrift? –
Fürwahr, so ist es; aber wir denken gewöhnlich nicht daran.
Daß einst aber, bei kräftigerer / Menschennatur, wirklich mehr daran gedacht wurde, beweist das 'Dasein' von Wort und Schrift.
Ihre erste, und zwar absolute, Gleichzeitigkeit lag darin, daß das Sprachorgan selbst schreibt, um zu sprechen.
Nur der Buchstabe spricht, oder besser: Wort und Schrift sind gleich an ihrem Ursprünge eins, und keines ohne das andere möglich.  (228-229)

Auch in der Erscheinungswelt noch sieht man Wort und Schrift beständig unzertrennlich.
Alle Elektrizitätserregung ist mit Oszillation begleitet, wenn sie immerhin auch beiden Isolatoren nur äußerlich scheint.
Im Grunde aber ist keine, auch die innere, ohne äußere.
Alle Oszillation aber gibt Ton, und damit Wort.
Aber die erregte Elektrizität projiziert sich überall sogleich 'Gestalt'-formierend, ja die Gestalt geht ihr selbst 'voran', und wohnt schon ihrer 'Erregung' bei; sie findet sie in dem Maße vor, als sie selbst auftritt.
Es ist direkte Klangfigur, und beinahe könnte man sagen, sie sei nur zufällig zugleich elektrische; wiewohl umgekehrt jede Klangfigur eine elektrische, und / jede elektrische eine Klangfigur ist.
Die 'Lichtenberg'’schen Figuren sind nichts als Klangfiguren in der Normalerscheinung. (229-230)

Ich habe dir schon längst einmal geschrieben, wie + (oder * ) und ○ sich in den ältesten Alphabeten als Anfangs- und Endbuchstabe derselben vorfinden.
Dies sind die beiden 'Lichtenberg'schen Figuren in ihrer einfachsten Gestalt; ich wollte hierauf also die 'Ur'- oder 'Natur'schrift auf elektrischem Wege wiederfinden oder doch suchen, fand aber in der Tat schon von + (Abbreviatur von * ) bis ⊗ (Theta, Menschen-, Gottesbuchstabe), in jenen alten Alphabeten mehr 'dendritische', 'Pflanzen'- Figuren, und von ⊗ bis mehr ○ 'gerundete', 'Thier'-Figuren.
Zugleich sind + und ○ auf die Extreme der 'Octave', – die, in der 'Sprache', sich als Verhältnis des 'Selbstlauters' zur 'Konsonante' ausdrückt.
Und da nur 'beide zusammen' einen 'Ton' geben, so erhält die Chiffre desselben: ⊗, sogar 'bildliche' Bedeutung.
Der Hauch x wird von ○ zurückgeworfen; ist Selbst-, ○ Mitlauter; von ○ wird x wieder nach dem Zentrum oder Ausgangspunkt c zurückgeworfen, und 'sich selbst' offenbar.
Es gilt hier die völlige Konstruktion des Bewußtseins, und einst war alles Bewußtsein Laut oder Ton, wie vornehmlich 'Herder' ganz herrlich zeigte.
Daß wir jetzt denken und bewußt sind, 'ohne' dabei laut mitzusprechen, ist eine Abstraktion aus bloßer Konvention, – (auch mag uns hindern, daß unsere 'Sprache' nicht mehr die natürliche ist, wir also lieber stottern und schweigen), – und seitdem 'denken' wir auch gar nicht mehr so energisch.
Den noch völligen Naturmenschen hören wir wirklich noch jetzt überall zugleich mitsprechen, singen, schreien, usw., und selbst der Gebildete lobt, zur Erweckung seines höchsten Bewußtseins, Gott 'laut'. (Kirchengesang, usw.)



.. ich wollte hierauf also die U r - oder N a t u r schrift auf elektrischem Wege wiederfinden oder doch suchen ... (230)

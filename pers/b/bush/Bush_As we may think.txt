–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
Vannevar Bush, As we may think, in: LIFE 19 (19. Nov. 1945), Nr. 21, pp. 112-124.
–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
xxx
-------------------------------------


–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
Vannevar Bush, As we may think, in: The Atlantic Monthly 176 (Juli 1945), Nr. 1, pp. 101-108.
–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
The camera hound of the future wears on his forehead a lump a little larger than a walnut. It takes pictures 3 millimeters square, later to be projected or enlarged, which after all involves only a factor of 10 beyond present practice. The lens is of universal focus, down to any distance accommodated by the unaided eye, simply because it is of short focal length. There is a built-in photocell on the walnut such as we now have on at least one camera, which automatically adjusts exposure for a wide range of illumination. There is film in the walnut for a hundred exposures, and the spring for operating its shutter and shifting its film is wound once for all when the film clip is inserted. It produces its result in full color. … The cord which trips its shutter may reach down a man’s sleeve within easy reach of his fingers. A quick squeeze, and the picture is taken. On a pair of ordinary glasses is a square of fine lines near the top of one lens, where it is out of the way of ordinary vision. When an object appears in that square, it is lined up for its picture. As the scientist of the future moves about the laboratory or the field, every time he looks at something worthy of the record, he trips the shutter and in it goes, without even an audible click. Is this all fantastic? (102)
-------------------------------------


»There is a new profession of trail blazers, those who find delight in the task of establishing useful trails through the enormous mass of the common record. The inheritance from the master becomes, not only his additions to the world's record, but for his disciples the entire scaffolding by which they were erected.« (Bush 1945, p. 40 -- cit. Porombka, Hypertext, pp. 147-148)

Der Film hat dieses Prinzip der alten räumlichen Küste – die Distanz und die abgesonderte Geschlossenheit des Kunstwerks – zerstört. Die bewegliche Kamera nimmt mein Auge, und damit mein Bewusstsein, mit: mitten in das Bild, mitten in den Spielraum der Handlung hinein. Ich sehe nichts von außen. Ich sehe alles so, wie die handelnden Personen es sehen müssen. Ich bin umzingelt von den Gestalten des Films und dadurch verwickelt in seine Handlung. Ich gehe mit, ich fahre mit, ich stürze mit – obwohl ich körperlich auf demselben Platz sitzen bleibe.

Béla Belázs, Zur Kunstphilosophie des Films (1938), in: F.-J. Albersmeier (ed), Theorie des Films, Stuttgart: Reclam 1995, pp. 204-226, p. 215

Balazs, Bela: Zur Kunstphilosophie des Films. In: Das Wort. 1938, Heft 3.

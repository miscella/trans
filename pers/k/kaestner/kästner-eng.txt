THE 35TH OF MAY; OR, CONRAD'S RIDE TO THE SOUTH SEAS. By Erich Kaestner. Illustrated by Walter Trier. English Version by Cyrus Brooks. 192 pp. New York: Dodd, Mead & Co. $2.
NOV. 11, 1934

Erich Kästner
Cyrus Brooks (tr.)
Walter Trier (ill.)

The 35th of May, or Conrad's ride to the South Seas
London: Cape 1933
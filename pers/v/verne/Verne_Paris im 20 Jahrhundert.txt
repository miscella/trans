–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
Jules Verne, Paris im 20. Jahrhundert. Roman, Wien: Zsolnay 1996.
–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
Fünftes Kapitel: Wo von Rechenmaschinen die Rede ist sowie von sich selbst verteidigenden Registriermaschinen

Am nächsten Tag ging Michel Dufrénoy um acht Uhr in die Büroräume der Bank Casmodge & Co.; sie lagen an der Rue Neuve-Drouot in jener Häuser, die an der Stelle der alten Oper erbaut worden waren… (46)

… Wie man sieht, trat er in ein Bankhaus ein, das Hilfe brauchen konnte und das alle Möglichkeiten der Mechanik nutzte. Im übrigen verlieh die Fülle der Geschäfte, die Vielfalt der Korrespondenz der schlichten Büroausstattung eine außergewöhnliche Bedeutung. 
So bestand die Post des Hauses Casmodage aus nicht weniger als dreitausend Briefen pro Tag, die in alle Ecken der Welt verschickt wurden. Eine Lenoir-Maschine mit der Kraft von fünfzehn Pferden wurde nicht müde, diese Briefe abzuschreiben, die ihr fünfhundert Angestellte pausenlos zuschickten.
Und doch hätte die elektrische Telegraphie die Anzahl von Briefen beträchtlich senken müssen, denn neueste Entwicklungen erlaubten es dem Absender, mit dem Empfänger direkt in Verbindung zu treten; das Briefgeheimnis war auf diese Weise gewahrt, und die stattlichsten Geschäfte wurden auf Distanz abgewickelt. Jedes Haus besaß seine eigenen Drähte, nach dem in ganz England schon lange Zeit üblichen Wheatstone-System. Die Kurse der unzähligen Wertpapiere, die auf dem freien Markt notiert wurden, erschienen von ganz allein auf großen Scheiben, welche im Zentrum der Börsen von Paris, London, Frankfurt, Amsterdam, Turin, Berlin, Wien, Sankt Petersburg, Konstantinopel, New York, Valparaiso, Kalkutta, Sydney, Peking und Nuka-hiva standen.
Die photographische Telegraphie, die im vergangenen Jahrhundert von Professor Giovanni Caselli aus Florenz erfunden worden war, erlaubte es überdies, das Faksimile jedes beliebigen Schriftstücks, Handschrift oder Zeichnung, / in weiteste Fernen zu schicken und Wechsel oder Verträge über fünftausend Meilen hinweg zu unterzeichnen.
Das telegraphische Netz überzog also die gesamte Erdoberfläche sowie den Meeresgrund; Amerika war keine Sekunde von Europa entfernt, und in einem feierlichen Versuch, der 1903 in London durchgeführt worden war, korrespondierten zwei Experimentatoren miteinander, nachdem sie ihre Nachricht rund um die Erdkugel gesandt hatten. (48/49)


Vierzehntes Kapitel: Das Große Dramatische Depot

In jener Zeit, in der alles zentralisiert wurde, das Denken genauso wie die mechanische Kraft, lag es natürlich nahe, ein 'Dramatisches Depot' zu schaffen; praktische und geschäftige Männer wurden vorstellig und bekamen im Jahre 1903 das Recht zur Gründung dieser bedeutenden Gesellschaft zugesprochen. Doch zwanzig Jahre später ging sie in die Hände der Regierung über und wurde einem Generaldirektor und Staatsrat unterstellt. Die fünfzig Theater der Hauptstadt versorgten sich hier mit Stücken aller Art; einige waren vorfabriziert, andere wurden auf Bestellung angefertigt, dieses einem bestimmten Schauspieler zuliebe, jenes gemäß einer bestimmten Idee. (148)

Die Gründung des 'Großen Dramatischen Depots' brachte den lärmenden Verein der Autoren zum Verschwinden; die Angestellten der Gesellschaft bezogen ihre, übrigens ziemlich hohen, monatlichen Gehälter, und der Staat kassierte die Einnahmen. (149)

Alles verlief nun also in geregelten Bahnen, wie es sich für zivilisierte Leute gehört; die beamteten Autoren lebten gut und arbeiteten nicht bis zur Erschöpfung; keine Dichter-Bohemiens mehr, keine notleidenden Genies, die ewig gegen die Ordnung der Dinge aufzubegehren schienen; hätte man also über diese Organisation klagen können, welche die Persönlichkeit der Menschen vernichtete und dem Publikum die für seine Bedürfnisse notwendige Menge Literatur lieferte? (150)

So lenkte auch Michel Dufrénoy, das Empfehlungsschreiben in der Hand, seine Schritte zum 'Großen Depot', welches per Einlaß als gemeinnützige Einrichtung anerkannt war. … Michel wurde zum Direktor vorgelassen. Dieser war ein Mann von größter Ernsthaftigkeit, ganz und gar durchdrungen von der Wichtigkeit seiner Ämter; er lachte niemals… / Der Direktor sagte: ›… auf Neuheiten legen wir keinerlei Wert; hier muß jede Persönlichkeit verschwinden; Sie werden mit einem riesigen Ensemble verschmelzen müssen, das mittelmäßige Werke hervorbringt…‹ (150 / 151)

Das 'Große Dramatische Depot' besaß eine ganz wundervolle Organisation. Es bestand aus fünf großen Abteilungen:
1. anspruchsvolle Komödie und Genrestück
2. Vaudeville im eigentlichen Sinn
3. historisches und modernes Schauspiel
4. Oper und Opera buffa
5. Revue, Märchendrama und offizielles Gelegenheitsstück (152)

Michel bekam ein Büro in der ersten Abteilung zugewiesen. Hier saßen talentierte Angestellte, einer war zuständig für die Expositionen, ein anderer für die Auflösungen, dieser für die Abgänge, jener für die Auftritte der verschiedenen Figuren; einem unterstand das Büro für volltönende Reime, wenn unbedingt Verse gewünscht wurden, einem anderen anderen das Fachgebiet der geläufigen Reime für einfache Handlungsdialoge. Es gab auch eine Sondergruppe von Beamten, unter denen MIchel Platz nehmen durfte; diese übrigens sehr tüchtigen Angestellten hatten zur Aufgabe, die Stücke der vorigen Jahrhunderte zu überarbeiten, entweder indem sie diese ganz einfach abschrieben, oder indem sie die Rollen umkehrten. (153)

Zwei Wochen, nachdem Michel Dufrénoy beim 'Großen Dramatischen Depot' angefangen hatte, wechselte er von der Abteilung Komödie zur Abteilung Schauspiel. Diese umfaßte das große historische Schauspiel und das moderne Schauspiel: Das erste bestand aus zwei völlig unterschiedlichen historischen Abteilungen; einer, in der die wirkliche, ernstzunehmende Geschichte Wort für Wort bei den guten Autoren abgeschrieben wurde; und einer anderen, in der die Geschichte auf schändlichste Weise verdreht und entstellt wurde, getreu dem Grundsatz eines großen Dramatikers aus dem 19. Jahrhundert: Man muß die Geschichte vergewaltigen, um ihr ein Kind zu machen. Und man machte ihr eines nach dem anderen, die allesamt keinerlei Ähnlichkeit mit ihrer Mutter hatten! (155) 

Paul Valery, "... das zu beschreiben, was sich von selbst einschreiben kann" Vortrag, gehalten zur Hundertjahrfeier der Fotografie am 7. Januar 1939, in: Fotogeschichte 6 (1986), H. 20, pp. 5-8.

Ich habe die Beobachtung gemacht, daß in dem Moment, als die Fotografie aufkam, die deskriptive Schreibweise begann, die Literatur zu überfluten. Sowohl in der Dichtung wie in der Prosa haben das Dekor und die äußerlichen Aspekte des Lebens einen nahezu exzessiven Raum in den Werken eingenommen. (6)

Jedes Ereignis des Daseins wird auf einem Negativ festgehalten. (6)

Wenn Platon die Öffnung seiner Grotte auf ein ganz kleines Loch reduziert hätte und wenn er die Wand, die ihm als Bildschirm diente, mit einem empfindlichen Überzug versehen hätte, dann hätte er beim Entwickeln seines Höhlenhintergrundes einen gigantischen Film erhalten; und weiß Gott, welche erstaunlichen Schlußfolgerungen über die Natur unserer Erkenntnis und über das Wesen unserer Ideen er uns hinterlassen hätte ... (8)

Und gibt es eine philosophischere Empfindung als die, welche man unter dem diabolischen Rotlicht haben kann, das aus dem Glühen einer Zigarette einen grünen Diamanten macht, während man ängstlich das Eintreten des sichtbaren Zustandes jenes mysteriösen 'latenten Bildes' erwartet, über dessen Wesen die Wissenschaft sich noch nicht endgültig im klaren ist. (8)

Nach und nach, hier und dort erscheinen einige Flecken, ganz ähnlich dem Gestammel eines erwachenden Wesens. Diese Fragmente werden zahlreicher, fließen zusammen und vervollständigen sich; und unwillkürlich fühlt man sich vor diesem zunächst diskontinuierlichen Entstehungsprozeß, der sprungweise und mit unbedeutenden Elementen vor sich geht, aber zu einer erkennbaren Gesamtkomposition hindrängt, an viele Vorgänge erinnert, die im Geist beobactet werden können: an Erinnerungen, die Konturen annehmen; an Gewißheiten, die sich ganz plötzlich herauskristallisieren; oder an die Produktion von bestimmten Versen, die von der inneren Sprache plötztlich gebildet und freigesetzt werden. (8)

Muß man das Universum von nun an nicht als ein schlichtes Produkt derjenigen Mittel definieren, über die der Mensch in dieser Epoche verfügt, um sich Ereignisse sichtbar zu machen, die unendlich und fern sind? (8)





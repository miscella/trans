–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
Sephan Porombka, Hypertext. Zur Kritik eines digitalen Mythos, München: Wilhelm Fink 2001.
–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
#Was elektronische Texte auszeichnet, ist deshalb nicht mehr ihre Form, sondern ihre Formbarkeit.# (p. 144)
-------------------------------------

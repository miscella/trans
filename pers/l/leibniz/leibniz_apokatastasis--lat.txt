–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
Max Ettlinger, Leibniz als Geschichtsphilosoph. Festrede bei der 50jährigen Reichsgründungsfeier der Westfälischen Wilhelms-Universität zu Münster am 18. Januar 1921. Mit Beigabe eines bisher unveröffentlichten Leibnizfragments über ›Die Wiederherstellung aller Dinge‹ (Apokatastasis panton). München: Kösel & Puster, 1921.
–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

Apokatastasis (27--34)

Definiri postet Numerus omnium Librorum possibilium determinatam magnitudinem non excedentium ex vocabulis significantibus vel non significantibus constantium, qui proinde etiam onmnes Libros 'sensum habentes' comprehendet.

Librum determinatae magnitudinis voco qui non excedat certum numerum literarum.
Exempli causa sit liber in folio constans 10 000 paginis pagina 100 lineis, linea 100 literis, erit liber 100 000 000 literarum. Et determinatae magnitudinis dicentur, qui hunc non excedent.

Jam finitus est numerus Librorum possibilium tantam magnitudinem non excedentium, seu qui ad summum centum millionibus 'literarum alphabeti' formari possunt.

Nec tantum finitus est hic numerus, sed etiam ex calculo combinationum inveniri potest quot possibiles sint libri inter se vel minimum differentes, propositium literarum numerum non excedentes, magni vel parvi. Hic numerus vocetur N.

Ponamus porro 'Historiam publicam annalem Orbis terrarum' tantae magnitudinis libro, qui contineat centum, milliones literarum, vel etiam minore, sufficienter, describi posse.

Squitur Historiam quoque Orbis terrae publicarum possibilium inter se differentium numerum esse definitum, neque excedere numerum N, quaevic, enim novum librum daret. (27)

Si iam ponatur genus humanum satis diu durare in statu qualis nunc est, ut Historiam publicarum materiam praebere possit, necesse est aliquando priores Historias publicas exacte redire, quod sic demonstro.

Assumatur numerus annorum maior numero N, quo genus humanum in tali statu duret.
Utique durantibus annis N, aut novae quovis anno contingere Historiae, differentes quantumlibet ab Historia cuiuslibet anni praecedentis; aut alicuius anni historia historiae alicuius anni praecedentis exacte consentit.
Si posterius factum est, habeum intentum; sin prius sequitur exhaustas esse hoc annorum numero omnes Historias publicas possibiles, et quia (ex hypothesi) genus humanum ultra numerum annorum N durat, sequitur annis post numerum N sequentibus redire exacte Historias priores. Q.E.D.

Sed satis apparet, eandem ratiocinationem valere, sie descendatur ad Historiam privatam, tantumque opus foret concipi animo librum maiorem, annosque assumi plures.

Utique enim possibilis est liber sufficientis magnitudinis, etsi plurium voluminum, in quo describantur minutissime, quae homines singuli in toto orbe terrarum – intra annum gessere.

Fingantur esse in orbe mille hominum milliones (a quo tamen numero longissime abest humanum genus) et homini cuilibet ad unum vitae eius annum minutatim describendum assignetur tantus liber quantum Historiae publicae annuae tribuimus, nempe constans ex centum millionibus literarum; eum utique sufficere manifestum est. (28)

Nam etsi anno tribucrentur 10 000 horae cuilibet tamen unius hominis horae describendae suppeterent 10 000 literae, seu pagina 100 linearum, quarum quaevis sit 100 literarum.

 100        10000
100        10000 
----      --------
10000   100000000

Ergo Opus continens Historiam annalem totius generis humani ad minutissima descendentem sufficiet non excedere numerum literarum qui perveniat ad centies mille Milliones Millionionum, seu ad centies mille Millionioniones.

Nam compendij causa 'Millionio' mihi est Millio Millionum, et 'Millionionio' mihi est Millio Millionionum, et ita porro.
Marcus Polus qui ex Cataja Venetias redierat, cuius extat descriptio itinerum, dicebatur 'Messer Millione' vel Dominus Millio, quod non nisi per Milliones loqueretur ingentibusque numerius perstreperet: id nobis quoque hic faciendum est.

Porro etiam possibilis numerus operum aliqatenus inter se differentium in quibus multitudo literarum centis mile Milliones Millionionum non excedat, finitus est, imo ex calculo combinationum numerus adhuc maior facile assignari potest. Is numerus vocetur L.

Si iam ponatur genus humanum satis diu durare in statu qualis nunc est, seu qui materiam historiae suppeditet; necesse est affore tempus quo singulorum vita per annum integrum minutatim per easdem corcumstantias redeat quod demonstrabitur eodem modo ratiocinando circa numerum L quo paulo ante ratiocinati sumus circa numerum N. (29)

Nempe sumto numero annorum paulo maiore quam numerus L, necesse est contingere, ut aliquando redeat aliquis annus generis humani totus qualis iam antea fuit, cum omnibus circumstantiis suis.

Et pari modo demonstrari potest fore tempus quo integrum redeat seculum; immo quo integer millenarius annorum; vel etiam integer millio, aut millionio.

Etsi autem ex solo calculo demonstrari non possit rediturum praecise Leopoldum I. aut Lidovicum XIV. aut me vel alium privatum; quoniam si aliqui alij repetantur saepius, non est necesse repeti omnes.

Quia tamen rationibus Metaphysicis constat praesens esse gravidum futuro; iudicari potest uno seculo exacte statis redeunte, etiam plura satis exacte reditura, quia redeuntibus fere iisdem causis redire consentaneum est eosdem fere effectus.

Interium etsi redeat prius seculum quoad sensibilia seu quae libris describi possunt, non tamen redibit omnino quoad omnia : semper enim forent discrimina etsi imperceptibilia et quae nullis satis libris describi possint.

Quia continuum in partes actu infinitas divisum est, adeoque in quavis parte materiae mundus est infinitarum Creaturarum qui describi nequit libro quantocunque. (30)

Sane si corpora constarent ex Atomis, omnia redirent praecise in eadem collectione Atomorum; quandiu novae Atomi aliunde non admiscerentur; veluti si poneretur Mundus Epicuri ab aliis per Intermundia separatus.
Sed ita talis Mundus foret machina quem Creatura finitae perfectionis perfecte cognoscere posset, quod in mundo vero locum non habet.

Et ob hanc rationem fieri posset, ut res paulatim etsi imperceptibiliter in melius proficerent post revolutiones.

Quaeri etiam posset an ii quorum Historia plus semel repretenda esset, iidem futir forent, eadem numero anima (paulatim fortasse proficiente) praediti; an revera diversi, etsi valde similes.

Sed talia calculis definiri non possunt, pertinentque ad doctrinam de congruentia rerum, seu de eo quod optimum est, maximeque conveniens Sapientiae Divinae.

Atque hae revolutiones, durante in hoc statu genere humano non semel tantum sed saepius, imo numero vicum maiore, quam qui assignari potest fierent. Quod veteres quoque in animo habuisse videntur; qui de 'Anno Magno Platonico' locuti sunt; etsi non satis intellecti fuerint, quia rationes sententiae suae ad posteros non transmiserunt, sed quae ex dictis manifesta est.

Caeterum vel ex his iduicari potest genus humanum semper in hoc statu non esse mansurum; quia Divinae Harmoniae consentanteum non est eadem semper chorda oberrare: Credendumque est vel ex naturalibus congruentiae rationibus res vel paulatium, vel etiam aliquando per saltus in melius proficere debere. (31)

Quanquam enim subinde in peius ire videantur, hoc ad eum modum fieri putandum est, quo interdum recedimus ut maiore impetu prosiliamus.

Denique etiamsi non semper duraturum sit quale nunc est genus humanum; modo tamen semper ponamus existere mentes veritatem cognoscentes est indagantes; sequitur aliquando mentes eo perventuras, ut veritates a sensuum testimonio independentes seu Theoremata purae scientiae, quae scilicet exacte per rationes demonstrari possunt, quae iam inventa sunt, magnitudinem (verbi gratia paginae si scribantur) non excedentia; et multo magis breves sententiae quae verbis scribi possunt; redire necesse sit.

Itaque nova theoremata invenienda oporteret crescere magnitudine in infinitum, quemadmodum videmus quasdam esse Propositiones Geometricas satis longas, et tamen pulchras.

Hoc si fieret, necesse foret mentes etiam quae nondum statis capaces essent, fieri capaciores, ut tam magna theoremata capere et exogitare possent.

Qualibus etiam opus foret ad cognoscendam penitius naturam, veritates Physicas revocando ad Mathematicas, v. g. ad cognoscendam machinam animalis, ad contingentia verto verisimilitudinis gradu, atque adeo etiam ad mira quaedam in natura praestandum, quae nunc superant humanum captum.
Si enim muscam consideremus ut subjectum scientiae, quemadmodum tale subjectum est circulus, patet definitionem Muscae, quae structuram eius exhibeat immenso excessu magis compositam esse quam definitionem circuli.
Atque adeo prolixa fore theoremata de musca demonstranda, et magis adhuc de tali specie muscae, ne quid iam de individuis dicam, quae et ipsa cuidam Semiscientiae subjiciuntur, qua opus est cum venitur a theoria ad praxin. (32)

Ad veritates sensibiles seu quae non pura ratione sed in totum vel pro parte experientia constant, variari possunt in infinitum, etsi non fiant prolixiores. Itaque novam semper ac novam scientiae seu theorematibus magnitudine crescendibus materiam suppeditare possunt.

Cujus rei ratio est, quod sensiones in perceptione confusa consistunt, quae infinitis modis variari potest salva brevitate, posuntque esse infinitae species viventium, sensuum, sensibilium.
Quod secus est in veritatibus quae adaequate seu perfecta demonstratione cognosci possunt, quae cum verbis explicari possint, multitudinem habent pro ratione magnitudinis limitatam.

Et quaevis mens horizontem praesentis suae circa scientias capacitatis habet, nullum futurae. (32)

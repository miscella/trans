Das im Rahmen von Leibniz’ Briefwechsel mit Adam Theobald Overbeck überlieferte und mit diesem in direktem Zusammenhang stehende Fragment wurde 1921 von Max Ettlinger als Anhang zu einer Festrede ediert und erscheint bislang weder in der Akademieausgabe noch in einer anderen Leibniz-Werkausgabe.


In Gottfried Wilhelm Leibniz’ (1646–1716) Nachlass  ndet sich ein in zwei handschriftlichen Fassungen unterschiedlicher Länge überliefertes Fragment, das mit dem biblischen und auf gnostische Weltwiederkunftsspekulationen anspielenden Ausdruck 'Apokat£stasij beziehungsweise 'Apokat£stasij p£ntwn überschrieben ist und in dem eine Universalbibliothek imaginiert wird.



Gottfried Wilhelm Leibniz, Geschichte als Wiederkehr des Gleichen? Das Apokatastasis-Fragment, in: Schriften und Briefe zur Geschichte, Malte-Ludolf Babin/ Gerd van den Heuvel (eds.), Hannover: Hahnsche Buchhandlung 2004, pp. 550–561.

Übersetzungen aus dem lat. Malte-Ludolf Babin.
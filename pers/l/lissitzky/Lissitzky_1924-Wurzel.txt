–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
El Lissitzky, 1924 Wurz.(+ unendlich -) = NASCI, Merz, Nr. 8/9, April/Juli 1924
–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
  Es ist schon GENUG immer MASCHINE
                           MASCHINE
                           MASCHINE,
wenn man bei der modernen Kunstproduktion anlangt.
-------------------------------------
Die moderne Kunst ist auf ganz intuitiven und selbstständigen Wegen zu denselben Resultaten gekommen wie die moderne Wisenschaft. Sie hat, wie die Wissenschaft, die Form bis auf ihre Grundelemente zerlegt, um sie nach den universellen Gesetzen der Natur wieder aufzubauen. Und dabei sind beide zu derselben Formel gekommen: #JEDE FORM IST DAS ERSTARRTE MOMENTBILD EINES PROZESSES. ALSO IST DAS WERK HALTESTELLE DES WERDENS UND NICHT ERSTARRTES ZIEL.# (El Lissitzky, Maler, Architekt, Typograf, Fotograf. Erinnerungen, Briefe, Schriften, Dresden: VEB Verlag der Kunst 1967, p. 348)
-------------------------------------